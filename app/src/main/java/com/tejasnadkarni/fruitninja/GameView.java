package com.tejasnadkarni.fruitninja;

import android.content.Context;
import android.graphics.Canvas;

import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class GameView extends View {
    private int score;

    private Random rand = new Random();
	private Timer mTimer;
	private View self;
    private Context myContext;
    private CopyOnWriteArrayList<Fruit> fruits = new CopyOnWriteArrayList<>();

    public GameView(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
        myContext = context;
		score = 0;

		self = this;
		mTimer = new Timer();
	}

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                self.postInvalidate();

                // Adding fruits
                if (fruits.size() < 3) {
                    fruits.add(new Fruit(myContext, self));
                }
            }
        }, 0, 100);
    }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction() & MotionEvent.ACTION_MASK;
		if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE) {
			int index = event.getActionIndex();
            for (Fruit f: fruits) {
               if (f.hit(event.getX(index), event.getY(index))) {
                   fruits.remove(f);
                   score += 20;
               }
            }
            this.postInvalidate();
		}
        return true;
	}

	protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (Fruit f: fruits) {
            f.update();
            if (getHeight() < f.getY()) {
                fruits.remove(f);

            }
            else {
                f.draw(canvas);
            }
        }
    }
}

